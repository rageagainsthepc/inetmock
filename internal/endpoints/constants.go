package endpoints

import "time"

const (
	shutdownTimeout = 5 * time.Second
)
