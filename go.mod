module gitlab.com/inetmock/inetmock

go 1.15

require (
	github.com/golang/mock v1.4.4
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.2
	github.com/miekg/dns v1.1.31
	github.com/olekukonko/tablewriter v0.0.4
	github.com/prometheus/client_golang v1.7.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/elazarl/goproxy.v1 v1.0.0-20180725130230-947c36da3153
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
